<?php
    
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun");
echo "Name          : " . $sheep->type;
echo "<br>";
echo "legs          : " . $sheep->legs;
echo "<br>";
echo "cold blooded  : " . $sheep->cold_bloded;
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name          : " . $kodok->type;
echo "<br>";
echo "legs          : " . $kodok->legs;
echo "<br>";
echo "cold blooded  : " . $kodok->cold_bloded;
echo "<br>";
echo "Jump          : " . $kodok->jump();
echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name          : " . $sungokong->type;
echo "<br>";
echo "legs          : " . $sungokong->legs;
echo "<br>";
echo "cold blooded : " . $sungokong->cold_bloded;
echo "<br>";
echo "Yell          :" . $sungokong->yell();
?>